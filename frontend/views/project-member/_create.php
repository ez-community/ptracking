<?php

use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $project
 * @var array $users
 */
?>

<div class="panel-body">

	<?php $form = ActiveForm::begin(['action' =>['project-member/create', 'pid' => $project->id]]); ?>
		
		<div class="row">
			<div class="col-lg-6">
				<?=	Select2::widget([
					'name' => 'members',
					'data' => $users,
					'options' => [
						'placeholder' => 'Select one or more members...',
						'multiple' => true
					],
				]);	?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<?= Html::submitButton('Add', ['type' => 'button', 'data-loading-text' => 'Please wait...', 'class' => 'btn btn-primary']) ?>
			</div>
		</div>

	<?php ActiveForm::end(); ?>

</div>

