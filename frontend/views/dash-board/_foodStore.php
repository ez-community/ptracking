<?php

use ez\helpers\DateTime;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<strong>Food store</strong>
	</div>
	<div class="panel-body">
		<?php echo GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-hover',
			],
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				
				[
					'attribute' => 'name',
					'format' => 'raw',
					'value' => function ($model, $index, $widget) {
						return Html::a($model->name, ['food-store/view', 'id' => $model->id]);
					}
				],
				'contact',
				'address',
				'tel',
				'mobile',
				[
					'attribute' => 'updateTime',
					'value' => function ($model, $index, $widget) {
						return DateTime::timeAgo($model->updateTime);
					}
				],
				[
					'attribute' => 'authorId',
					'value' => function ($model, $index, $widget) {
						return $model->author->name;
					}
				],
				[
					'label' => '',
					'format' => 'raw',
					'value' => function ($model, $index, $widget) {
						$updateUrl = Html::a('<span class="glyphicon glyphicon-pencil"></span> ',
							['food-store/update', 'id' => $model->id],
							[
								'class' => 'tip',
								'title' => 'Update',
								'data-toggle' => 'modal',
								'data-target' => '#popup-modal',
							]
						);
						$deleteUrl = Html::a('<span class="glyphicon glyphicon-trash"></span> ',
							['food-store/delete', 'id' => $model->id],
							[
								'class' => 'tip',
								'title' => 'Delete',
								'data-toggle' => 'modal',
								'data-target' => '#popup-modal',
							]
						);
						
						return ($updateUrl . $deleteUrl);
					}
				],
			]
		]); ?>
	</div>
	<div class="panel-footer clearfix">
		<?= Html::a('Add', ['food-store/create'], [
			'class' => 'btn btn-default pull-right',
			'data-toggle' => 'modal',
			'data-target' => '#popup-modal',
		]) ?>
	</div>
</div>
