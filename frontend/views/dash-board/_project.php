<?php

use ez\helpers\DateTime;
use ez\widgets\Modal;
use ez\widgets\LiveFilter;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<?php
Modal::begin([
	'clearData' => true,
	'id' => 'popup-modal',
	'clientOptions' => [
		'modal' => true,
		'autoOpen' => false,
	],
]);
Modal::end();
?>

<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<strong>Project</strong>
		<div class="col-sm-3 col-md-3 pull-right">
		<?=	LiveFilter::widget([
			'name' => 'filter',
			'selection' => '.grid-view tbody tr',
			'options' => [
				'placeholder' => 'Search...',
			]
		]);	?>
		</div>
	</div>
	<div class="panel-body">
		<?php echo GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-hover',
			],
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				
				[
					'attribute' => 'name',
					'format' => 'raw',
					'value' => function ($model, $index, $widget) {
						return Html::a($model->name, ['project/view', 'id' => $model->id]);
					}
				],
				'startDate',
				'endDate',
				[
					'attribute' => 'updateTime',
					'value' => function ($model, $index, $widget) {
						return DateTime::timeAgo($model->updateTime);
					}
				],
				[
					'attribute' => 'foodStoreId',
					'value' => function ($model, $index, $widget) {
						$foodStore = $model->foodStore;
						if ($foodStore != null) {
							return $foodStore->name;
						} else {
							return null;
						}
					}
				],
				[
					'attribute' => 'authorId',
					'value' => function ($model, $index, $widget) {
						return $model->author->name;
					}
				],
				[
					'label' => '',
					'format' => 'raw',
					'value' => function ($model, $index, $widget) {
						$updateUrl = Html::a('<span class="glyphicon glyphicon-pencil"></span> ',
							['project/update', 'id' => $model->id],
							[
								'class' => 'tip',
								'title' => 'Update project',
								'data-toggle' => 'modal',
								'data-target' => '#popup-modal',
							]
						);
						$assignmentUrl = Html::a('<span class="glyphicon glyphicon-eye-open"></span> ',
							['project-assignment/manage', 'projectId' => $model->id],
							[
								'class' => 'tip',
								'title' => 'Assignment management',
							]
						);
						$memberUrl = Html::a('<span class="glyphicon glyphicon-user"></span> ',
							['project-member/manage', 'projectId' => $model->id],
							[
								'class' => 'tip',
								'title' => 'Member management',
							]
						);
						$buildUrl = Html::a('<span class="glyphicon glyphicon-phone"></span> ',
							['project-build/manage', 'projectId' => $model->id],
							[
								'class' => 'tip',
								'title' => 'Build management',
							]
						);
						
						return ($updateUrl . $assignmentUrl . $memberUrl . $buildUrl);
					}
				],
			]
		]); ?>
	</div>
</div>
