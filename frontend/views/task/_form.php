<?php

use common\models\Priority;
use common\models\ProjectBuild;
use common\models\ProjectMember;
use common\models\TaskAttachment;
use common\models\TaskType;
use ez\widgets\SwitchButton;
use kartik\markdown\MarkdownEditor;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\widgets\Typeahead;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Task $model
 * @var integer $projectBuildId
 * @var integer $assigneeId
 * @var string $header
 */
?>

<?php
$builds = ProjectBuild::getArrayByProjectId($model->project->id);
$members = ProjectMember::getArrayByProjectId($model->project->id);
$attachment = new TaskAttachment();
?>

<?= $this->render('//layouts/_navbar', ['project' => $model->project, 'controller' => 'task', 'typeId' => $model->typeId]) ?>

<?php $form = ActiveForm::begin([
    'id' => 'task-form',
    'options' => [
    	'class' => 'form-horizontal',
    	'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
		'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-8 col-md-offset-2\">{error}</div>",
		'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>

<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<strong><?= $header ?></strong>
	</div>
	<div class="panel-body">
		<?= $form->field($model, 'name')->widget(Typeahead::className(), [
			'options' => [
				'placeholder' => 'Type then pick one of suggestions...'
			],
			'dataset' => [
				[
					'remote' => Html::url(['task/suggest', 'projectId' => 1]) . '&name=%QUERY',
					'limit' => 10
				]
			]
		]) ?>
		
		<?= $form->field($model, 'description')->widget(MarkdownEditor::classname(), [
			'height' => 100,
			'encodeLabels' => true,
			'toolbar' => [
				[
					'buttons' => [
						MarkdownEditor::BTN_BOLD => ['icon' => 'bold', 'title' => 'Bold'],
						MarkdownEditor::BTN_ITALIC => ['icon' => 'italic', 'title' => 'Italic'],
					],
				],
				[
					'buttons' => [
						MarkdownEditor::BTN_LINK => ['icon' => 'link', 'title' => 'URL/Link'],
						MarkdownEditor::BTN_IMAGE => ['icon' => 'picture', 'title' => 'Image'],
					],
				],
				[
					'buttons' => [
						MarkdownEditor::BTN_INDENT_L => ['icon' => 'indent-left', 'title' => 'Indent Text'],
						MarkdownEditor::BTN_INDENT_R => ['icon' => 'indent-right', 'title' => 'Unindent Text'],
					],
				],
				[
					'buttons' => [
						MarkdownEditor::BTN_UL => ['icon' => 'list', 'title' => 'Bulleted List'],
						MarkdownEditor::BTN_OL => ['icon' => 'list-alt', 'title' => 'Numbered List'],
					],
				],
				[
					'buttons' => [
						MarkdownEditor::BTN_QUOTE => ['icon' => 'comment', 'title' => 'Block Quote'],
					],
				],
				[
					'buttons' => [
						MarkdownEditor::BTN_CODE => ['label' => MarkdownEditor::ICON_CODE, 'title' => 'Inline Code', 'encodeLabel' => false],
						MarkdownEditor::BTN_CODE_BLOCK => ['icon' => 'sound-stereo', 'title' => 'Code Block'],
					],
				],
				[
					'buttons' => [
						MarkdownEditor::BTN_MAXIMIZE => ['icon' => 'fullscreen', 'title' => 'Toggle full screen', 'data-enabled' => true]
					],
					'options' => ['class' => 'pull-right']
				],
			],
			'footerMessage' => 'For format your input, see the ' . Html::a('Markdown Syntax', 'http://michelf.ca/projects/php-markdown/concepts/'),
			'showExport' => false,
		]); ?>

		<?= $form->field($model, 'startDate')->widget(DatePicker::className(), [
			'options' => ['placeholder' => 'Enter start date ...'],
			'pluginOptions' => [
				'autoclose'=>true,
				'format' => 'yyyy-mm-dd'
			]
		]) ?>
		
		<?= $form->field($model, 'endDate')->widget(DatePicker::className(), [
			'type' => DatePicker::TYPE_COMPONENT_APPEND,
			'options' => ['placeholder' => 'Enter end date ...'],
			'pluginOptions' => [
				'autoclose' => true,
				'format' => 'yyyy-mm-dd'
			]
		]) ?>
		
		<?php if ($projectBuildId == 0): ?>
			<?= $form->field($model, 'projectBuildId')->widget(Select2::className(), ['data' => $builds]) ?>
		<?php endif ?>
		
		<?php if ($assigneeId == 0): ?>
			<?= $form->field($model, 'assigneeId')->widget(Select2::className(), ['data' => $members]) ?>
		<?php endif ?>
		
		<?= $form->field($model, 'priorityId')->dropDownList(Priority::getArray()) ?>
		
		<?php if ($model->isNewRecord): ?>
		<?= $form->field($attachment, 'baseName')->widget(FileInput::className(), [
			'showUpload' => false,
			'showRemove' => false,
			'options' => [
				// 'multiple' => true,
			],
			'buttonOptions' => [
				'class' => 'btn btn-default',
			],
		]) ?>
		<?php endif ?>
	</div>
	
	<?php if ($model->typeId == TaskType::TYPE_TARGET): ?>
		<?= $this->render('_overtime', ['form' => $form, 'task' => $model]) ?>
	<?php endif ?>
	<div class="panel-footer clearfix">
		<div class="pull-right">
			<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' =>'btn btn-default']) ?>
			&nbsp;
			<?php if ($model->isNewRecord): ?>
				<?= Html::a('Cancel', ['index', 'projectId' => $model->projectId, 'typeId' => $model->typeId]) ?>
			<?php else: ?>
				<?= Html::a('Cancel', ['view', 'id' => $model->id]) ?>
			<?php endif ?>
		</div>
	</div>
</div>

<?php ActiveForm::end(); ?>
