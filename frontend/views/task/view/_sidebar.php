<?php

use common\models\Priority;
use common\models\Task;
use common\models\TaskStatus;
use ez\helpers\DateTime;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\Task $model
 */
$flows = TaskStatus::getWorkflow($model);
?>

<?php
// Create a Modal to delete the a task
Modal::begin([
	'id' => 'delete-modal',
	'clientOptions' => [
		'modal' => true,
		'autoOpen' => false,
		'remote' => Html::url(['delete', 'projectId' => $model->projectId, 'id' => $model->id])
	],
]);
Modal::end();
?>

<?= $this->render('view/_change', ['model' => $model]) ?>

<p class="text-right">
	<?= Html::a('Create ' . $model->type, ['create',
		'projectId' => $model->projectId, 'typeId' => $model->typeId
	]) ?>
</p>

<div class="btn-group">
	<?= Html::button($model->statusId == TaskStatus::STATUS_RESOLVED ? 'Open' : 'Resolved',	[
		'class' => 'btn btn-primary task-change',
		'data-title' => ucfirst(($model->statusId == TaskStatus::STATUS_RESOLVED ?
			'Open' : 'Resolved') . ' ' . $model->type),
		'data-status' => ($model->statusId == TaskStatus::STATUS_RESOLVED) ?
			TaskStatus::STATUS_OPEN : TaskStatus::STATUS_RESOLVED
	]) ?>
	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Workflow <span class="caret"></span></button>
	<ul class="dropdown-menu" role="menu">
		<?php foreach ($flows as $item): ?>
			<li>
				<?= Html::a(TaskStatus::getName($item), '#change', [
					'class' => 'task-change',
					'data-title' => ucfirst(TaskStatus::getName($item) . ' ' . $model->type),
					'data-status' => $item
				]) ?>
			</li>
		<?php endforeach ?>
	</ul>
</div>

<div class="btn-group">
	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">More <span class="caret"></span></button>
	<ul class="dropdown-menu" role="menu">
		<li>
			<?= Html::a('Delete', '#delete', [
				'data-toggle' => 'modal',
				'data-target' => '#delete-modal'
			]) ?>
		</li>
		<li>
			<?= Html::a('Attach files', '#attach', [
				'class' => 'task-change',
				'data-title' => ucfirst("Add {$model->type} attachments"),
				'data-attachment' => 'true'
			]) ?>
		</li>
	</ul>
</div>

<?= Html::a('Edit', ['update', 'id' => $model->id], [
	'class' => 'btn btn-default'
]) ?>

<p></p>
<div class="panel panel-default">
	<?= DetailView::widget([
		'model' => $model,
		'template' => "<tr class=\"row\">\n<td class=\"col-lg-5 text-right\">{label}</td>\n<td class=\"col-lg-6\">{value}</td>\n</tr>\n",
		'options' => [
			'class' => 'table table-hover',
		],
		'attributes' => [
			[
				'name' => 'Build',
				'format' => 'raw',
				'value' => Html::a($model->projectBuild->build->code, ['task/index',
					'projectId' => $model->projectId,
					'typeId' => $model->typeId,
					'projectBuildId' => $model->projectBuildId
				])
			],
			[
				'name' => 'Assignee',
				'format' => 'raw',
				'value' => Html::a($model->assignee->name, ['task/index',
					'projectId' => $model->projectId,
					'typeId' => $model->typeId,
					'assigneeId' => $model->assigneeId
				])
			],
			[
				'name' => 'Priority',
				'format' => 'raw',
				'value' => Html::a(Priority::getLabel($model->priorityId), ['task/index',
					'projectId' => $model->projectId,
					'typeId' => $model->typeId,
					'priorityId' => $model->priorityId
				])
			],
			[
				'name' => 'Status',
				'format' => 'raw',
				'value' => Html::a($model->status, ['task/index',
					'projectId' => $model->projectId,
					'typeId' => $model->typeId,
					'statusId' => $model->statusId
				])
			],
			[
				'name' => 'Start',
				'value' => DateTime::niceShort($model->startDate),
			],
			[
				'name' => 'End',
				'value' => DateTime::niceShort($model->endDate),
			],
		],
	]); ?>
</div>
