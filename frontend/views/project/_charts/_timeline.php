<?php

use ez\widgets\AmChart;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Project $model
 */
?>

<?php
$today = date(time()) - $model->startDate;
$remain = $model->endDate - date(time());

$today = new DateTime(date('Y-m-d'));
$startDate = new DateTime($model->startDate);
$endDate = new DateTime($model->endDate);

$todayWeeks = ceil($today->diff($startDate)->format("%a") / 7);
$totalWeeks = ceil($endDate->diff($startDate)->format("%a") / 7);
?>

<?= AmChart::widget([
	'id' => 'chart-timeline',
	'width' => '100%',
	'height' => '150px',
	'chart' => [
		'type' => 'serial',
		'dataProvider' => [
			['name'  => 'Kickoff', 'value' => 0.5],
			['name' => 'Today', 'value' => $todayWeeks],
			['name' => 'Deadline', 'value' => $totalWeeks]
		],
		'categoryField' => 'name',
		'startDuration' => 1,
		'columnWidth' => 0.02,
		
		'categoryAxis' => ['gridPosition' => 'start'],
		'valueAxes' => [['axisAlpha' => 0.2]],
		'graphs' => [
			[
				'type' => 'column',
				'title' => 'Weeks',
				'valueField' => 'value',
				'lineAlpha' => 0,
				'fillAlphas' => 1,
				'fillColors' => '#4f81bd',
				'balloonText' => '[[category]]:<b>[[value]]</b> weeks'
			],
		]
	],
]); ?>
