<?php

use common\models\TaskStatus;
use common\models\TaskType;
use ez\widgets\LiveFilter;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var Project $model
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<strong>Project</strong>
		<div class="col-sm-3 col-md-3 pull-right">
		<?=	LiveFilter::widget([
			'name' => 'filter',
			'selection' => '.grid-view tbody tr',
			'options' => [
				'placeholder' => 'Search...',
			]
		]);	?>
		</div>
	</div>
	<div class="panel-body">
		<?php echo GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-hover',
			],
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				
				[
					'attribute' => 'name',
					'format' => 'raw',
					'value' => function ($model, $index, $widget) {
						return Html::a($model->name, ['view', 'id' => $model->id]);
					}
				],
				'startDate',
				'endDate',
				[
					'label' => 'Member',
					'format' => 'raw',
					'value' => function ($model, $index, $widget) {
						return $model->summary['member'];
					}
				],
				[
					'label' => 'Build',
					'format' => 'raw',
					'value' => function ($model, $index, $widget) {
						return $model->summary['build'];
					}
				],
				[
					'label' => 'Target',
					'format' => 'raw',
					'value' => function ($model, $index, $widget) {
						return Html::a($model->summary['unresolved-target'] . ' unresolved', ['task/index',
							'projectId' => $model->id,
							'typeId' => TaskType::TYPE_TARGET,
							'status' => TaskStatus::STATUS_UNRESOLVED
						]);
					}
				],
				[
					'label' => 'Issue',
					'format' => 'raw',
					'value' => function ($model, $index, $widget) {
						return Html::a($model->summary['unresolved-issue'] . ' unresolved', ['task/index',
							'projectId' => $model->id,
							'typeId' => TaskType::TYPE_ISSUE,
							'status' => TaskStatus::STATUS_UNRESOLVED
						]);
					}
				],
				[
					'label' => 'Feedback',
					'format' => 'raw',
					'value' => function ($model, $index, $widget) {
						return Html::a($model->summary['unresolved-feedback'] . ' unresolved', ['task/index',
							'projectId' => $model->id,
							'typeId' => TaskType::TYPE_FEEDBACK,
							'status' => TaskStatus::STATUS_UNRESOLVED
						]);
					}
				],
			]
		]); ?>
	</div>
</div>
