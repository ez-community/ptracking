<?php

use common\models\BuildStatus;
use ez\helpers\DateTime;
use ez\widgets\LiveFilter;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $project
 * @var yii\db\ActiveDataProvider $dataProvider
 */

$this->title = $project->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('//layouts/_navbar', ['project' => $project, 'controller' => 'project-build']) ?>

<div class="panel panel-default">
	<div class="panel-footer clearfix">
		<strong>Build</strong>
		<div class="col-sm-3 col-md-3 pull-right">
		<?=	LiveFilter::widget([
			'name' => 'filter',
			'selection' => '.grid-view tbody tr',
			'options' => [
				'placeholder' => 'Search...',
			]
		]);	?>
		</div>
	</div>
	<div class="panel-body small">
		<?php echo GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-hover',
			],
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				[
					'attribute' => 'buildId',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a($model->build->code, ['view', 'id' => $model->id], [
							'class' => 'tip',
							'data-toggle' => 'tooltip',
							'title' => $model->build->name
						]);
					}
				],
				[
					'attribute' => 'feedback',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->feedback),
							['index', 'projectId' => $model->project->id, 'feedback' => $model->feedback],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'title' => 'Filter by feedback']
						);
					}
				],
				[
					'attribute' => 'alignment',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->alignment),
							['index', 'projectId' => $model->project->id, 'alignment' => $model->alignment],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'title' => 'Filter by alignment']
						);
					}
				],
				[
					'attribute' => 'gameplay',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->gameplay),
							['index', 'projectId' => $model->project->id, 'gameplay' => $model->gameplay],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'title' => 'Filter by gameplay']
						);
					}
				],
				[
					'attribute' => 'interrupt',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->interrupt),
							['index', 'projectId' => $model->project->id, 'interrupt' => $model->interrupt],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'title' => 'Filter by interrupt']
						);
					}
				],
				[
					'attribute' => 'igp',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->igp),
							['index', 'projectId' => $model->project->id, 'igp' => $model->igp],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'title' => 'Filter by igp']
						);
					}
				],
				[
					'attribute' => 'sound',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->sound),
							['index', 'projectId' => $model->project->id, 'sound' => $model->sound],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'title' => 'Filter by sound']
						);
					}
				],
				[
					'attribute' => 'iap',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->iap),
							['index', 'projectId' => $model->project->id, 'iap' => $model->iap],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'title' => 'Filter by IAP']
						);
					}
				],
				[
					'attribute' => 'igpLocalization',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->igpLocalization),
							['index', 'projectId' => $model->project->id, 'igpLocalization' => $model->igpLocalization],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'title' => 'Filter by IGP LOC']
						);
					}
				],
				[
					'attribute' => 'localization',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->localization),
							['index', 'projectId' => $model->project->id, 'localization' => $model->localization],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'title' => 'Filter by LOC']
						);
					}
				],
				[
					'attribute' => 'wjiap',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->wjiap),
							['index', 'projectId' => $model->project->id, 'wjiap' => $model->wjiap],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'wjiap' => 'Filter by WJIAP']
						);
					}
				],
				[
					'attribute' => 'mrc',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->mrc),
							['index', 'projectId' => $model->project->id, 'mrc' => $model->mrc],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'mrc' => 'Filter by MRC']
						);
					}
				],
				[
					'attribute' => 'tns',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->tns),
							['index', 'projectId' => $model->project->id, 'tns' => $model->tns],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'mrc' => 'Filter by TNS']
						);
					}
				],
				[
					'attribute' => 'gc',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(BuildStatus::getLabel($model->gc),
							['index', 'projectId' => $model->project->id, 'gc' => $model->gc],
							['class' => 'tip', 'data-toggle' => 'tooltip', 'mrc' => 'Filter by GC']
						);
					}
				],
				[
					'attribute' => 'updateTime',
					'value' => function($model, $index, $widget) {
						return DateTime::timeAgo($model->updateTime);
					}
				],
			],
		]); ?>
	</div>
</div>
