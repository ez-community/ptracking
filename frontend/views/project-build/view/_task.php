<?php

use common\models\BuildStatus;
use common\models\Priority;
use common\models\Role;
use common\models\TaskStatus;
use ez\helpers\DateTime;;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var common\models\ProjectBuild $model
* @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'tableOptions' => [
		'class' => 'table table-hover small',
	],
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],

		[
			'attribute' => 'name',
			'format' => 'raw',
			'value' => function($model, $index, $widget) {
				$name = $model->name;
				return Html::a($name, ['task/view', 'id' => $model->id]);
			}
		],
		[
			'attribute' => 'statusId',
			'format' => 'raw',
			'value' => function($model, $index, $widget) {
				return ('<small>' . TaskStatus::getLabel($model->statusId) . '</small>');
			}
		],
		[
			'attribute' => 'priorityId',
			'format' => 'raw',
			'value' => function($model, $index, $widget) {
				return Priority::getText($model->priorityId);
			}
		],
		/* [
			'attribute' => 'authorId',
			'value' => function($model, $index, $widget) {
				return ($model->author->name);
			}
		], */
		[
			'attribute' => 'assigneeId',
			'value' => function($model, $index, $widget) {
				return $model->assignee->name;
			}
		],
		/* [
			'attribute' => 'createTime',
			'value' => function($model, $index, $widget) {
				return DateTime::timeAgo($model->createTime);
			}
		], */
		[
			'attribute' => 'updateTime',
			'value' => function($model, $index, $widget) {
				return DateTime::timeAgo($model->updateTime);
			}
		],
		[
			'attribute' => 'modifierId',
			'value' => function($model, $index, $widget) {
				return $model->modifier->name;
			}
		],
	],
]); ?>
