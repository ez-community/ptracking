<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-6">
            <?php $form = ActiveForm::begin(['id' => 'form-signup',
                'options' => [
                    'class' => 'form-horizontal',
                    'enctype' => 'multipart/form-data'
                ],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-8 col-md-offset-4\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-4 control-label'],
                ],
            ]); ?>
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'Enter your email']) ?>
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Enter your password']) ?>
                <?= $form->field($model, 'passwordVerify')->passwordInput(['placeholder' => 'Enter your password again']) ?>
                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Enter your name']) ?>
                <?= $form->field($model, 'avatar')->fileInput(['class' => 'form-control']) ?>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'options' => ['class' => 'form-control'],
                    'template' => '<div class="row"><div class="col-lg-4">{image}</div><div class="col-lg-8">{input}</div></div>',
                ]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
