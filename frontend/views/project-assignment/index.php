<?php

use ez\helpers\DateTime;
use ez\helpers\JSRegister;
use ez\widgets\LiveFilter;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $project
 */

$this->title = $project->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;

JSRegister::registerTooltip($this, '.build');
?>

<?= $this->render('//layouts/_navbar', ['project' => $project, 'controller' => 'project-assignment']) ?>

<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<strong>Assigment</strong>
		<div class="col-sm-3 col-md-3 pull-right">
		<?=	LiveFilter::widget([
			'name' => 'filter',
			'selection' => '.grid-view tbody tr',
			'options' => [
				'placeholder' => 'Search...',
			]
		]);	?>
		</div>
	</div>
	<div class="panel-body">	
		<?php echo GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-hover',
			],
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				[
					'attribute' => 'projectMemberId',
					'value' => function($model, $index, $widget) {
						return $model->projectMember->member->name;
					}
				],
				[
					'attribute' => 'projectBuildId',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::tag('abbr', $model->projectBuild->build->code, [
							'class' => 'build',
							'data-toggle' => 'tooltip',
							'title' => $model->projectBuild->build->name
						]);
					}
				],
				[
					'attribute' => 'authorId',
					'value' => function($model, $index, $widget) {
						return $model->author->name;
					}
				],
				[
					'attribute' => 'createTime',
					'value' => function($model, $index, $widget) {
						return DateTime::timeAgo($model->createTime);
					}
				],
			],
		]); ?>
	</div>
</div>
