<?php

use common\models\ProjectBuild;
use common\models\ProjectMember;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $project
 */

$builds = ProjectBuild::getArrayByProjectId($project->id);
$members = ProjectMember::getArrayByProjectId($project->id, false);
?>

<?php if (count($members) > 0 && count($builds) > 0): ?>
<div class="panel-body">

	<?php $form = ActiveForm::begin(['action' =>['project-assignment/create', 'projectId' => $project->id]]); ?>
		
		<div class="row">
			<div class="col-lg-6">
				<?=	Select2::widget([
					'name' => 'members',
					'data' => $members,
					'options' => [
						'placeholder' => 'Select one or more members...',
						'multiple' => true
					],
				]);	?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-6">
				<?=	Select2::widget([
					'name' => 'builds',
					'data' => $builds,
					'options' => [
						'placeholder' => 'Select one build...',
						'multiple' => true,	//use multiple selection to keep the right index instead of 0..n
						'maximumSelectionSize' => 1
					],
				]);	?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<span class="input-group-btn">
					<?= Html::submitButton('Add', ['type' => 'button', 'data-loading-text' => 'Please wait...', 'class' => 'btn btn-primary']) ?>
				</span>
			</div>
		</div>
		
	<?php ActiveForm::end(); ?>

</div>
<?php endif ?>
