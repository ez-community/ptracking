<?php

use ez\helpers\DateTime;
use ez\widgets\LiveFilter;
use ez\widgets\Modal;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var common\models\Project $project
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = $project->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
Modal::begin([
	'clearData' => true,
	'id' => 'popup-modal',
	'clientOptions' => [
		'modal' => true,
		'autoOpen' => false,
	],
]);
Modal::end();
?>

<?= $this->render('//layouts/_navbar', ['project' => $project, 'controller' => 'overtime']) ?>

<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<?= Html::a('Today records', ['index', 'projectId' => $project->id, 'workDate' => date('Y-m-d')], ['class' => 'btn btn-default']) ?>
		<?= Html::a('All records', ['index', 'projectId' => $project->id], ['class' => 'btn btn-default']) ?>
		<div class="col-sm-3 col-md-3 pull-right">
		<?=	LiveFilter::widget([
			'name' => 'filter',
			'selection' => '.grid-view tbody tr',
			'options' => [
				'placeholder' => 'Search...',
			]
		]);	?>
		</div>
	</div>
	<div class="panel-body">
		<?php echo GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-hover',
			],
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				[
					'attribute' => 'taskId',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a($model->task->name, ['task/view', 'id' => $model->taskId]);
					}
				],
				[
					'label' => 'Assignee',
					'value' => function($model, $index, $widget) {
						return $model->task->assignee->name;
					}
				],
				[
					'label' => 'Build',
					'value' => function($model, $index, $widget) {
						return $model->task->projectBuild->build->code;
					}
				],
				[
					'attribute' => 'workDate',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(DateTime::niceShort($model->workDate), ['index', 'projectId' => $model->projectId, 'workDate' => $model->workDate]);
					}
				],
				'startTime',
				'endTime',
				[
					'attribute' => 'menuId',
					'value' => function($model, $index, $widget) {
						$menu = $model->menu;
						if ($menu != null) {
							return $model->menu->name;
						} else {
							return null;
						}
					}
				],
				[
					'label' => '',
					'format' => 'raw',
					'value' => function ($model, $index, $widget) {
						$updateUrl = Html::a('<span class="glyphicon glyphicon-pencil"></span> ',
							['overtime/update', 'id' => $model->id],
							[
								'class' => 'tool-tip',
								'title' => 'Update',
								'data-toggle' => 'modal',
								'data-target' => '#popup-modal',
							]
						);
						$deleteUrl = Html::a('<span class="glyphicon glyphicon-trash"></span> ',
							['overtime/delete', 'id' => $model->id],
							[
								'class' => 'tool-tip',
								'title' => 'Delete',
								'data-toggle' => 'modal',
								'data-target' => '#popup-modal',
							]
						);
						
						return ($updateUrl . $deleteUrl);
					}
				],
			],
		]); ?>
	</div>
	<div class="panel-footer clearfix">
		<?php
			$params = Yii::$app->getRequest()->getQueryParams();
			$action[] = 'mail';				// Can't use ArrayHelper::merge() method
			foreach ($params as $key => $value) {
				$action[$key] = $value;
			}
			
			echo Html::a('Email', $action, [
				'class' => 'btn btn-default pull-right', 'data-toggle' => 'modal', 'data-target' => '#popup-modal'
			]);
		?>
	</div>
</div>
