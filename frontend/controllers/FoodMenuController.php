<?php

namespace frontend\controllers;

use common\models\FoodMenu;
use common\models\Role;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * FoodMenuController implements the CRUD actions for FoodMenu model.
 */
class FoodMenuController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'only' => ['create', 'update', 'delete'],
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['create', 'update', 'delete'],
						'roles' => [Role::ROLE_MANAGER],
					],
				],
			],
		];
	}
	
	/**
	 * Creates a new FoodMenu model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate($foodStoreId)
	{
		$model = new FoodMenu();
		$model->foodStoreId = $foodStoreId;

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['food-store/view', 'id' => $foodStoreId]);
		} else {
			return $this->renderAjax('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing FoodMenu model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param string $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['food-store/view', 'id' => $model->foodStoreId]);
		} else {
			return $this->renderAjax('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing FoodMenu model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param string $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		if (Yii::$app->request->getIsPost()) {
			$model->delete();
			return $this->redirect(['food-store/view', 'id' => $model->foodStoreId]);
		} else {
			return $this->renderAjax('delete', ['model' => $model]);
		}
	}

	/**
	 * Finds the FoodMenu model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param string $id
	 * @return FoodMenu the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if ($id !== null && ($model = FoodMenu::find($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
