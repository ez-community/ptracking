<?php

namespace frontend\controllers;

use common\models\FoodStore;
use common\models\Project;
use common\models\Role;
use yii\data\ActiveDataProvider;

class DashBoardController extends \yii\web\Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\web\AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => [Role::ROLE_MANAGER],
					],
				],
			],
		];
	}
	
	public function actionIndex()
	{
		$projectDataProvider = new ActiveDataProvider([
			'query' => Project::find()
				->orderBy('endDate DESC')
		]);
		$foodStoreDataProvider = new ActiveDataProvider([
			'query' => FoodStore::find()
				->orderBy('updateTime DESC')
		]);
		return $this->render('index', [
			'projectDataProvider' => $projectDataProvider,
			'foodStoreDataProvider' => $foodStoreDataProvider,
		]);
	}

}
