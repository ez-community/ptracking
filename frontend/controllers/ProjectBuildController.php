<?php

namespace frontend\controllers;

use common\models\BuildStatus;
use common\models\Project;
use common\models\ProjectBuild;
use common\models\TaskStatus;
use common\models\TaskType;
use common\models\Role;
use common\models\search\ProjectBuildSearch;
use common\models\search\TaskSearch;
use yii\data\ActiveDataProvider;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * ProjectBuildController implements the CRUD actions for ProjectBuild model.
 */
class ProjectBuildController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index', 'view'],
						'roles' => ['@'],
					],
					[
						'allow' => true,
						'actions' => ['manage', 'create', 'update', 'delete'],
						'roles' => [Role::ROLE_MANAGER],
					],
				],
			],
		];
	}
	
	/**
	 * Displays all build assigments of a project.
	 * @param integer $projectId		project id, require.
	 * @return mixed
	 */
	public function actionIndex($projectId)
	{
		$searchModel = new ProjectBuildSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
		
		return $this->render('index', [
			'project' => Project::find($projectId),
			'dataProvider' => $dataProvider,
		]);
	}
	
	/**
	 * Displays all build assigments of a project, add update + delete.
	 * @param string $projectId project id
	 * @return mixed
	 */
	public function actionManage($projectId)
	{
		$searchModel = new ProjectBuildSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
		
		return $this->render('manage', [
			'project' => Project::find($projectId),
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single ProjectBuild model.
	 * @param string $id
	 * @return mixed
	 */
	public function actionView($id, $feedback = null)
	{
		$model = $this->findModel($id);
		$searchModel = new TaskSearch();
		$targetProvider = $searchModel->search([
			'projectId' => $model->projectId,
			'projectBuildId' => $id,
			'typeId' => TaskType::TYPE_TARGET,
			'status' => TaskStatus::STATUS_UNRESOLVED
		]); 
		$feedbackProvider = $searchModel->search([
			'projectId' => $model->projectId,
			'projectBuildId' => $id,
			'typeId' => TaskType::TYPE_FEEDBACK,
			'status' => TaskStatus::STATUS_UNRESOLVED
		]); 
		$issueProvider = $searchModel->search([
			'projectId' => $model->projectId,
			'projectBuildId' => $id,
			'typeId' => TaskType::TYPE_ISSUE,
			'status' => TaskStatus::STATUS_UNRESOLVED
		]);
		
		return $this->render('view', [
			'model' => $model,
			'targetProvider' => $targetProvider,
			'feedbackProvider' => $feedbackProvider,
			'issueProvider' => $issueProvider,
		]);
	}

	/**
	 * Creates a new ProjectBuild model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate($projectId)
	{
		$builds = Yii::$app->getRequest()->post('builds');
		$count = 0;
		
		foreach ($builds as $build) {
			$model = new ProjectBuild();
			$model->projectId = $projectId;
			$model->buildId = $build;
			
			if (!ProjectBuild::isExist($model)) {
				// \yii\helpers\VarDumper::dump($model, 10, true); exit();
				$model->save();
				$count++;
			}
		}
		
		Yii::$app->session->setFlash('info', "Added {$count} build(s) successful.");
		return $this->redirect(['manage', 'projectId' => $projectId]);
	}
	
	/**
	 * Update a ProjectBuild model.
	 * @TODO AJAX action.
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->session->setFlash('info', "Status updated.");
		}
		
		return $this->redirect(['view', 'id' => $id]);
	}
	
	/**
	 * Deletes an existing ProjectBuild model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param string $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		// if (!Yii::$app->getRequest()->getIsAjax()) {
		$model = $this->findModel($id);
		if (Yii::$app->request->getIsPost()) {
			$model->delete();
			return $this->redirect(['manage', 'projectId' => $model->projectId]);
		} else {
			return $this->renderAjax('delete', ['model' => $model]);
		}
	}

	/**
	 * Finds the ProjectBuild model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param string $id
	 * @return ProjectBuild the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = ProjectBuild::find($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
