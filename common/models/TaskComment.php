<?php

namespace common\models;

/**
 * This is the model class for table "TaskComment".
 *
 * @property integer $id
 * @property integer $taskId
 * @property boolean $name
 * @property boolean $isDescriptionChanged
 * @property integer $projectBuildId
 * @property integer $statusId
 * @property integer $priorityId
 * @property integer $assigneeId
 * @property integer $startDate
 * @property integer $endDate
 * @property integer $typeId
 * @property string $comment
 * @property string $authorId
 * @property string $createTime
 *
 * @property Task $task
 * @property User $assignee
 * @property User $author
 */
class TaskComment extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'TaskComment';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['taskId', 'authorId'], 'required'],
			[['taskId', 'projectBuildId', 'assigneeId', 'typeId', 'priorityId', 'statusId', 'authorId'], 'integer'],
			[['startDate', 'endDate', 'createTime'], 'safe'],
			[['name'], 'string', 'max' => 255],
			[['comment'], 'string'],
			[['isDescriptionChanged'], 'boolean'],
			
			//['endDate', 'compare', 'compareAttribute' => 'startDate', 'operator' => '>=',
			//	'message'=>'{attribute} must be greater than or equal"{compareValue}".'
			//],
		];
	}
	
	/**
	 * Fill Project.authorId before call validate.
	 * @inheritdoc
	 */
	public function beforeValidate()
	{
		if ($this->isNewRecord) {
			$this->authorId = \Yii::$app->user->getId();
		}
		return parent::beforeValidate();
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getTask()
	{
		return $this->hasOne(Task::className(), ['id' => 'taskId']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getProjectBuild()
	{
		return $this->hasOne(ProjectBuild::className(), ['id' => 'projectBuildId']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAssignee()
	{
		return $this->hasOne(User::className(), ['id' => 'assigneeId']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAuthor()
	{
		return $this->hasOne(User::className(), ['id' => 'authorId']);
	}
	
	public function generateValues($task)
	{
		$this->taskId = $task->id;
		
		// Check dirty attributes (before call Task::save() method) & save a comment for changes
		//$dirtyAttributes = $model->getDirtyAttributes();
		//$names = array_keys($dirtyAttributes);
		$isChanged = false;
		if ($this->comment != null) {
			$isChanged = true;
		}
		
		if ($task->isAttributeChanged('name')) {
			$this->name = $task->name;
			$isChanged = true;
		}
		
		if ($task->isAttributeChanged('projectBuildId')) {
			$this->projectBuildId = $task->projectBuildId;
			$isChanged = true;
		}
		
		if ($task->isAttributeChanged('assigneeId')) {
			$this->assigneeId = $task->assigneeId;
			$isChanged = true;
		}
		if ($task->isAttributeChanged('typeId')) {
			$this->typeId = $task->typeId;
			$isChanged = true;
		}
		
		$task->statusId = (int) $task->statusId;	//Convert to integer before check (?)
		if ($task->isAttributeChanged('statusId')) {
			$this->statusId = $task->statusId;
			$isChanged = true;
		}
		if ($task->isAttributeChanged('name')) {
			$this->name = $task->name;
			$isChanged = true;
		}
		if ($task->isAttributeChanged('description')) {
			$this->isDescriptionChanged = $isChanged = true;
		}
		
		$task->priorityId = (int) $task->priorityId;	//Convert to integer before check (?)
		if ($task->isAttributeChanged('priorityId')) {
			$this->priorityId = $task->priorityId;
			$isChanged = true;
		}
		if ($task->isAttributeChanged('startDate')) {
			$this->startDate = $task->startDate;
			$isChanged = true;
		}
		if ($task->isAttributeChanged('endDate')) {
			$this->endDate = $task->endDate;
			$isChanged = true;
		}
		
		// \yii\helpers\VarDumper::dump($this, 10, true); exit();
		return $isChanged;
	}
}
