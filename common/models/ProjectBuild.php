<?php

namespace common\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ProjectBuild".
 *
 * @property integer $id
 * @property integer $projectId
 * @property integer $buildId
 * @property integer $feedback
 * @property integer $alignment
 * @property integer $gameplay
 * @property integer $interrupt
 * @property integer $igp
 * @property integer $sound
 * @property integer $iap
 * @property integer $igpLocalization
 * @property integer $localization
 * @property integer $wjiap
 * @property integer $mrc
 * @property integer $tns
 * @property integer $gc
 * @property integer $authorId
 * @property string $createTime
 * @property string $updateTime
 * 
 * @property Project $project
 * @property Build $build
 * @property ProjectAssignment[] $assignees
 * @property Member $author
 */
class ProjectBuild extends \yii\db\ActiveRecord
{
	private $_summary;
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'ProjectBuild';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['projectId', 'buildId', 'authorId'], 'required'],
			[
				['projectId', 'buildId', 'authorId',
					'feedback', 'alignment', 'gameplay',
					'interrupt', 'igp', 'sound', 'iap', 'igpLocalization',
					'localization', 'wjiap', 'mrc', 'tns', 'gc'
				],
				'integer'
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'projectId' => 'Project',
			'buildId' => 'Build',
			'authorId' => 'Added by',
			'createTime' => 'Create Time',
			'feedback' => 'FB',
			'alignment' => 'DA',
			'gameplay' => 'GP',
			'interrupt' => 'INT',
			'igp' => 'IGP',
			'sound' => 'SND',
			'iap' => 'IAP',
			'igpLocalization' => 'IGP LOC',
			'localization' => 'LOC',
			'wjiap' => 'WJIAP',
			'mrc' => 'MRC',
			'tns' => 'TNS',
			'gc' => 'GC',
			'createTime' => 'Create',
			'updateTime' => 'Update',
		];
	}
	
	/**
	 * Fill ProjectBuild.authorId before call validate.
	 * @inheritdoc
	 */
	public function beforeValidate()
	{
		if ($this->isNewRecord) {
			$this->authorId = Yii::$app->user->getId();
		}
		return parent::beforeValidate();
	}
	
	/**
	 * Get the Project model.
	 * @return \yii\db\ActiveRelation
	 */
	public function getProject()
	{
		return $this->hasOne(Project::className(), ['id' => 'projectId']);
	}
	
	/**
	 * Get the Build model.
	 * @return \yii\db\ActiveRelation
	 */
	public function getBuild()
	{
		return $this->hasOne(Build::className(), ['id' => 'buildId']);
	}
	
	/**
	 * Get the ProjectAssignment models.
	 * @return \yii\db\ActiveRelation
	 */
	public function getAssignees()
	{
		return $this->hasMany(ProjectAssignment::className(), ['projectBuildId' => 'id']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAuthor()
	{
		return $this->hasOne(User::className(), ['id' => 'authorId']);
	}
	
	/**
	 * Count some relative info.
	 * @return mixed
	 */
	public function getSummary()
	{
		if ($this->_summary == null) {
			$this->_summary = [
				'unresolved-target' => Task::find()
					->where(['projectBuildId' => $this->id, 'typeId' => TaskType::TYPE_TARGET])
					->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
					->count(),
				'unresolved-issue' => Task::find()
					->andWhere(['projectBuildId' => $this->id, 'typeId' => TaskType::TYPE_ISSUE])
					->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
					->count(),
				'unresolved-feedback' => $count = Task::find()
					->andWhere(['projectBuildId' => $this->id, 'typeId' => TaskType::TYPE_FEEDBACK])
					->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
					->count(),
				'unresolved-milestone' => Task::find()
					->andWhere(['projectBuildId' => $this->id, 'typeId' => TaskType::TYPE_MILESTONE])
					->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
					->count(),
			];
		}
		
		return $this->_summary;
	}
	
	/**
	 * Check the record was exists.
	 * @return boolean
	 */
	public static function isExist($model)
	{
		$projectBuild = static::find()
			->where(['buildId' => $model->buildId])
			->andWhere(['projectId' => $model->projectId])
			->one();
		$isExist = ($projectBuild != null);
		
		return $isExist;
	}
	
	/**
	 * Get the user lists.
	 * @TODO should base on the roles.
	 */
	public static function getArrayByProjectId($projectId)
	{
		$builds = static::find()
			->with('build')
			->where(['projectId' => $projectId])
			->all();
		
		$builds = ArrayHelper::map($builds, 'id', 'build.name');
		return $builds;
	}
}
