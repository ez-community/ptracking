<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about User.
 */
class UserSearch extends Model
{
	public $id;
	public $name;
	public $passwordHash;
	public $passwordResetToken;
	public $authKey;
	public $email;
	public $role;
	public $avatar;
	public $createTime;
	public $updateTime;

	public function rules()
	{
		return [
			[['id', 'role'], 'integer'],
			[['name', 'passwordHash', 'passwordResetToken', 'authKey', 'email', 'avatar', 'createTime', 'updateTime'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Name',
			'passwordHash' => 'Password Hash',
			'passwordResetToken' => 'Password Reset Token',
			'authKey' => 'Auth Key',
			'email' => 'Email',
			'role' => 'Role',
			'avatar' => 'Avatar',
			'createTime' => 'Create Time',
			'updateTime' => 'Update Time',
		];
	}

	public function search($params)
	{
		$query = User::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params, '') && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'id');
		$this->addCondition($query, 'name', true);
		$this->addCondition($query, 'passwordHash', true);
		$this->addCondition($query, 'passwordResetToken', true);
		$this->addCondition($query, 'authKey', true);
		$this->addCondition($query, 'email', true);
		$this->addCondition($query, 'role');
		$this->addCondition($query, 'avatar', true);
		$this->addCondition($query, 'createTime');
		$this->addCondition($query, 'updateTime');
		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
