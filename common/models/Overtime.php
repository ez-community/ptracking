<?php

namespace common\models;

/**
 * This is the model class for table "Overtime".
 *
 * @property string $id
 * @property string $projectId
 * @property string $taskId
 * @property string $workDate
 * @property string $startTime
 * @property string $endTime
 * @property string $menuId
 *
 * @property Project $project
 * @property Task $task
 * @property Foodmenu $menu
 */
class Overtime extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function __construct($config = [])
	{
		parent::__construct($config);
		$this->workDate = date('Y-m-d');
		$this->endTime = '20:00:00';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'Overtime';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['projectId', 'taskId', 'workDate', 'startTime', 'endTime'], 'required'],
			[['projectId', 'taskId', 'menuId'], 'integer'],
			[['workDate', 'startTime', 'endTime'], 'safe'],
			
			//['endTime', 'compare', 'compareAttribute' => 'startTime', 'operator' => '>',
			//	'message'=>'{attribute} must be greater than {compareValue}.'
			//],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'projectId' => 'Project',
			'taskId' => 'Task',
			'workDate' => 'Date',
			'startTime' => 'Start',
			'endTime' => 'End',
			'menuId' => 'Food',
		];
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getProject()
	{
		return $this->hasOne(Project::className(), ['id' => 'projectId']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getTask()
	{
		return $this->hasOne(Task::className(), ['id' => 'taskId']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getMenu()
	{
		return $this->hasOne(FoodMenu::className(), ['id' => 'menuId']);
	}
}
