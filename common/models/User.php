<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property string $id
 * @property string $email
 * @property string $passwordHash
 * @property string $passwordResetToken
 * @property string $authKey
 * @property string $activationToken
 * @property string $name
 * @property string $avatar
 * @property string $role
 * @property string $loggedTime
 * @property string $status
 * @property string $createTime
 * @property string $updateTime
 */
class User extends ActiveRecord implements IdentityInterface
{
    const PATH_AVATAR = 'upload/avatars';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'User';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::find()
            ->where(['email' => $email])
            ->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire    = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts     = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            '$passwordResetToken' => $token,
            'status'              => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by activation token
     *
     * @param string $token activation token
     * @return static|null
     */
    public static function findByActivationToken($token)
    {
        $expire    = Yii::$app->params['user.activationTokenExpire'];
        $parts     = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'activationToken' => $token,
            'status'          => self::STATUST_NOT_ACTIVE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->passwordHash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->passwordHash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates new active token
     */
    public function generateActivationToken()
    {
        $this->activationToken = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->passwordResetToken = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes activation token
     */
    public function removeActivationToken()
    {
        $this->activationToken = null;
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->passwordResetToken = null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'password' => 'Password',
            'email' => 'Email',
            'role' => 'Role',
            'avatar' => 'Avatar',
            'createTime' => 'Create Time',
            'updateTime' => 'Update Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->authKey = Security::generateRandomKey();
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        $auth = Yii::$app->authManager;
        if (!$auth->isAssigned($this->id, $this->role)) {
            $auth->assign($this->id, $this->role);
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    public function getAvatarImg($width = 32, $height = 32)
    {
        $filename = ($this->avatar != null) ? $this->avatar : 'default.png';
        $src = self::PATH_AVATAR . "/{$filename}";
        return Html::img($src, [
                'width' => $width,
                'height' => $height,
                'title' => $this->name
        ]);
    }

    /**
     * @return \yii\db\ActiveRelation
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['authorId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveRelation
     */
    public function getAssignedTasks($filter)
    {
        return $this->hasMany(Task::className(), ['assigneeId' => 'id']);
    }

    /**
     * Get the project url.
     */
    public function getUrl()
    {
        $url = Yii::$app->urlManager->createUrl('user/view', ['id' => $this->id]);
        return Html::a($this->name, $url);
    }

    /**
     * Count some relative info.
     * @return mixed
     */
    public function getSummary()
    {
        $summary = [
            'unresolved-target' => Task::find()
                ->where(['typeId' => TaskType::TYPE_TARGET, 'assigneeId' => $this->id])
                ->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
                ->count(),

            'unresolved-issue' => Task::find()
                ->where(['typeId' => TaskType::TYPE_ISSUE, 'assigneeId' => $this->id])
                ->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
                ->count(),
            'unresolved-feedback' => $count = Task::find()
                ->where(['typeId' => TaskType::TYPE_FEEDBACK, 'assigneeId' => $this->id])
                ->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
                ->count(),
            'unresolved-milestone' => Task::find()
                ->where(['typeId' => TaskType::TYPE_MILESTONE, 'assigneeId' => $this->id])
                ->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
                ->count(),
            //'build' => ProjectAssignment::find()
            //  ->where(['assigneeId' => $this->id])
            //  ->count(),
        ];

        return $summary;
    }

    /**
     * Get the user lists which don't show.
     * @TODO should base on the roles.
     */
    private static function getNotInId($projectId)
    {
        $admin = [1];
        $members = ProjectMember::find()
            ->select('memberId')
            ->where('projectId=:projectId', [':projectId' => $projectId])
            ->column();

        return ArrayHelper::merge($admin, $members);
    }

    /**
     * Get the user lists.
     * @TODO should base on the roles.
     */
    public static function getArrayByProjectId($projectId)
    {
        $members = static::getNotInId($projectId);
        $users = static::find()
            ->select(['id', 'name'])
            ->where(['not in', 'id', $members])
            ->all();

        $userNames = ArrayHelper::map($users, 'id', 'name');
        return $userNames;
    }

    public static function getAvatarRealPath($filename)
    {
        $path = Yii::getAlias('@app') . '/web/' . self::PATH_AVATAR . "/{$filename}";
        return $path;
    }
}
