<?php

use common\models\Task;
use common\models\TaskType;
use frontend\models\TaskEmail;
use yii\helpers\Html;
use yii\widgets\ListView;

/**
 * @var yii\web\View $this
 * @var frontend\models\TaskEmail $model;
 * @var yii\data\ActiveDataProvider $dataProvider;
 */
$taskCounter = 0;

$params = Yii::$app->getRequest()->getQueryParams();
$action[] = 'task/index';				// Can't use ArrayHelper::merge() method
foreach ($params as $key => $value) {
	$action[$key] = $value;
}
$url = '#'; //Yii::$app->urlManager->createAbsoluteUrl($action);
?>

<table width="100%" id="background-table" border="0" cellspacing="0" cellpadding="0">
  <tbody>
	<tr>
	  <td align="center" bgcolor="#c7c7c7">
		<table width="840" class="w640" style="margin: 0px 10px;" border="0" cellspacing="0" cellpadding="0">
		  <tbody>
			<tr>
			  <td width="840" height="20" class="w640"></td>
			</tr>
			<tr>
			  <td width="840" class="w640">
				<table width="840" class="w640" id="top-bar" bgcolor="#000000" border="0" cellspacing="0" cellpadding="0">
				  <tbody>
					<tr>
					  <td width="15" class="w15"></td>
					  <td width="350" align="left" class="w325" valign="middle">
						<table width="350" class="w325" border="0" cellspacing="0" cellpadding="0">
						  <tbody>
							<tr>
							  <td width="350" height="8" class="w325"></td>
							</tr>
						  </tbody>
						</table>
						<div class="header-content">
						  <webversion><?= Html::a('Web version', $url) ?></webversion>
						</div>
						<table width="350" class="w325" border="0" cellspacing="0" cellpadding="0">
						  <tbody>
							<tr>
							  <td width="350" height="8" class="w325"></td>
							</tr>
						  </tbody>
						</table>
					  </td>
					  <td width="30" class="w30"></td>
					  <td width="255" align="right" class="w255" valign="middle">
						<table width="255" class="w255" border="0" cellspacing="0" cellpadding="0">
						  <tbody>
							<tr>
							  <td width="255" height="8" class="w255"></td>
							</tr>
						  </tbody>
						</table>
						<table border="0" cellspacing="0" cellpadding="0">
						  <tbody>
							<tr>
							  <td valign="middle">
								<div class="header-content">
								  <forwardtoafriend lang="en"><?= Html::mailto('Contact', $model->from->email) ?></forwardtoafriend>
								</div>
							  </td>
							</tr>
						  </tbody>
						</table>
						<table width="255" class="w255" border="0" cellspacing="0" cellpadding="0">
						  <tbody>
							<tr>
							  <td width="255" height="8" class="w255"></td>
							</tr>
						  </tbody>
						</table>
					  </td>
					  <td width="15" class="w15"></td>
					</tr>
				  </tbody>
				</table>
			  </td>
			</tr>
			<tr>
			  <td width="800" align="center" class="w640" id="header" bgcolor="#000000">
				<table width="800" class="w640" border="0" cellspacing="0" cellpadding="0">
				  <tbody>
					<tr>
					  <td width="30" class="w30"></td>
					  <td width="580" height="30" class="w580"></td>
					  <td width="30" class="w30"></td>
					</tr>
					<tr>
					  <td width="30" class="w30"></td>
					  <td width="580" class="w580">
						<div align="center" id="headline">
						  <p>
							<strong>
							  <singleline label="Title"><?= $model->project->name ?></singleline>
							</strong>
						  </p>
						</div>
					  </td>
					  <td width="30" class="w30"></td>
					</tr>
				  </tbody>
				</table>
			  </td>
			</tr>
			<tr>
			  <td width="800" height="30" class="w640" bgcolor="#ffffff"></td>
			</tr>
			<tr id="simple-content-row">
			  <td width="840" class="w640" bgcolor="#ffffff">
				<table width="840" class="w640" border="0" cellspacing="0" cellpadding="0">
				  <tbody>
					<tr>
					  <td width="30" class="w30"></td>
					  <td width="800" class="w580">
						<layout label="Text only">
						  <table width="800" class="w580" border="0" cellspacing="0" cellpadding="0">
							<tbody>
							  <tr>
								<td width="800" class="w580">
								  <p align="left" class="article-title">
									<singleline label="Title"><?= ucfirst(TaskType::getName($model->typeId)) ?></singleline>
								  </p>
								  <div align="left" class="article-content">
									<multiline label="Description"><?= $model->body ?></multiline>
								  </div>
<table class="tftable" border="1">
<tr>
	<th align="right">#</th>
	<th align="left">Name</th>
	<th align="left">Assignee</th>
	<th align="left">Priority</th>
	<th align="left">Build</th>
	<th align="left">ETA</th>
	<th align="left">Status</th>
</tr>
<?= ListView::widget([
	'dataProvider' => $dataProvider,
	'itemView' => '@common/mail/_taskrow',
	'layout' => "{items}\n"
]); ?>
</table>
								</td>
							  </tr>
							  <tr>
								<td width="580" height="10" class="w580"></td>
							  </tr>
							</tbody>
						  </table>
						</layout>
					  </td>
					  <td width="30" class="w30"></td>
					</tr>
				  </tbody>
				</table>				
			  </td>
			</tr>
			<tr>
			  <td width="800" height="15" class="w640" bgcolor="#ffffff"></td>
			</tr>
			<tr>
			  <td width="840" class="w640">
				<table width="840" class="w640" id="footer" bgcolor="#000000" border="0" cellspacing="0" cellpadding="0">
				  <tbody>
					<tr>
					  <td width="30" class="w30"></td>
					  <td width="360" height="30" class="w580 h0"></td>
					  <td width="60" class="w0"></td>
					  <td width="160" class="w0"></td>
					  <td width="30" class="w30"></td>
					</tr>
					<tr>
					  <td width="30" class="w30"></td>
					  <td width="420" class="w580" valign="top">
						<span class="hide">
						  <p align="left" class="footer-content-left" id="permission-reminder"><span>You're receiving this because you are a member of <b><?= $model->project->name ?></b>.</span>
						  </p>
						</span>
						<p align="left" class="footer-content-left">
						  <unsubscribe>Unsubscribe</unsubscribe>
						</p>
					  </td>
					  <td width="60" class="hide w0"></td>
					  <td width="420" class="hide w0" valign="top">
						<p align="right" class="footer-content-right" id="street-address"><span>This email was requested by <?= $model->from->name ?>.<br />Please don't reply this email.</span></p>
					  </td>
					  <td width="30" class="w30"></td>
					</tr>
					<tr>
					  <td width="30" class="w30"></td>
					  <td width="360" height="15" class="w580 h0"></td>
					  <td width="60" class="w0"></td>
					  <td width="160" class="w0"></td>
					  <td width="30" class="w30"></td>
					</tr>
				  </tbody>
				</table>
			  </td>
			</tr>
			<tr>
			  <td width="800" height="60" class="w640"></td>
			</tr>
		  </tbody>
		</table>
	  </td>
	</tr>
  </tbody>
</table>

