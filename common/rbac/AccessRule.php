<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This class represents an access rule defined by the [[AccessControl]] action filter
 */
class AccessRule extends \yii\web\AccessRule
{
	public static function isAuthor($user, $model)
	{
		if ($user->id == $model->authorId) {
			return true;
		}
		
		return false;
	}
	
	public static function isAssignee($user, $model)
	{
		if ($user->id == $model->assigneeId) {
			return true;
		}
		
		return false;
	}
}
