<?php
namespace console\controllers;

use common\models\Role;
use Yii;
use yii\console\Controller;
use yii\helpers\Security;

class RbacController extends Controller
{
	/**
     * Creates a new Member model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionHelp()
    {
		echo "Initialize RBAC by add the roles into db.\n";
       	echo "Action\n";
       	echo " setup\t run one before this site go live by Administrator.\n";
       	echo "Usage: yii rbac/setup\n";
    }
	
	public function actionSetup()
	{
		echo "Setup RBAC...\n";
		$auth = Yii::$app->getAuthManager();
		
		$role = $auth->createRole(Role::ROLE_MEMBER);
		
		$role = $auth->createRole(Role::ROLE_SUPEVISOR);
		$role->addChild(Role::ROLE_MEMBER);

		$role = $auth->createRole(Role::ROLE_MANAGER);
		$role->addChild(Role::ROLE_SUPEVISOR);
		
		$role = $auth->createRole(Role::ROLE_ADMINISTRATOR);
		$role->addChild(Role::ROLE_MANAGER);
		
		echo "Done!";
	}
}
