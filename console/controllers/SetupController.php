<?php
namespace console\controllers;

use \yii;
use \yii\console\Controller;
use \yii\db\Connection;

class SetupController extends Controller
{
	public function actionInit()
	{
		$dsn = Yii::$app->db->dsn;
		$username = Yii::$app->db->username;
		$password = Yii::$app->db->password;

		echo 'Connecting to the database -> ' . $dsn . "\n";

		$connection = new Connection([
			'dsn' => $dsn,
			'username' => $username,
			'password' => $password,
		]);

		try
		{
			$connection->open();
			$connection->close();
			echo ' -> Done';
		}
		catch (yii\db\Exception $e)
		{
//			echo($e->toString());
			echo "ERROR! The database does not exists or the dns config does not correct!\n";
		}
	}
}
