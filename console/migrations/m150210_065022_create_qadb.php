<?php

use yii\db\Schema;
use yii\db\Migration;

class m150210_065022_create_qadb extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('QADB', [
            'id'                    => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'projectId'             => 'INT UNSIGNED NOT NULL',
            'bugId'                 => 'INT UNSIGNED NOT NULL',
            'version'               => 'INT UNSIGNED NOT NULL',
            'severity'              => 'INT UNSIGNED NOT NULL',
            'priority'              => 'INT UNSIGNED NOT NULL',
            'checklist'             => 'INT UNSIGNED NOT NULL',
            'title'                 => 'VARCHAR(255) NOT NULL',
            'status'                => "ENUM(
                    'New', 'Opened', 'Pending', 'Reopened',
                    'To be fixed by master team', 'To be fixed by Support team', 'Cannot Test',
                    'Closed', 'Confirmed', 'HQ Confirmed', 'Needs HQ Confirmation', 'To Confirm', 'Needs CC Confirmation'
                ) DEFAULT 'New'",
            'result'                => "ENUM(
                    'Unresolved', 'Fixed', 'Will not Fix', 'To Fix in Update', 'Not a bug',
                    'Third Party', 'Could not Reproduce', 'Duplicate'
                ) DEFAULT 'Unresolved'",
            'fixerId'               => 'INT UNSIGNED',
            'assigneeId'            => 'INT UNSIGNED',
            'authorId'              => 'INT UNSIGNED NOT NULL',
            'updateTime'            => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);

        // Index
        $this->createIndex('IDX_QADB_BugId', 'QADB', 'bugId', true);
        $this->createIndex('IDX_QADB_Status', 'QADB', 'status', false);
        $this->createIndex('IDX_QADB_Result', 'QADB', 'result', false);
        $this->createIndex('IDX_QADB_FixerId', 'QADB', 'fixerId', false);
        $this->createIndex('IDX_QADB_AssigneeId', 'QADB', 'assigneeId', false);

        // Foreign key
        $this->addForeignKey('FK_QADB_ProjectId', 'QADB', 'projectId', 'Project', 'id', $delete = 'CASCADE', $update = 'CASCADE');
        $this->addForeignKey('FK_QADB_AuthorId', 'QADB', 'authorId', 'User', 'id', $delete = 'CASCADE', $update = 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('QADB');
    }
}
