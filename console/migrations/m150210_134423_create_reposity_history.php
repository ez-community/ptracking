<?php

use yii\db\Migration;

class m150210_134423_create_reposity_history extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('ReposityHistory', [
            'id'                    => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'reposityId'            => 'INT UNSIGNED NOT NULL',
            'version'               => 'VARCHAR(64) NOT NULL',
            'message'               => 'VARCHAR(255) NOT NULL',
            'author'                => 'VARCHAR(64) NOT NULL',
            'diff'                  => 'TEXT NOT NULL',
            'updateTime'            => 'DATETIME NOT NULL',
        ]);

        // Foreign key
        $this->addForeignKey('FK_ReposityHistory_ReposityId', 'ReposityHistory', 'reposityId', 'Reposity', 'id', $delete = 'CASCADE', $update = 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('ReposityHistory');
    }
}
